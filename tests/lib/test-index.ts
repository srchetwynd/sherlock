import test from 'ava';

test.serial('lib/index', (t) => {
    t.plan(1);
    t.notThrows(() => import('./../../src/lib/index.js'));
});
