import test from 'ava';
import { Time } from '../../src/lib/index.js';
import database from '../../src/lib/db/sqlite.js';

function clearDB(): void {
    database.exec(`
        DELETE FROM time_tags;
        DELETE FROM times;
        DELETE FROM tags;
        DELETE FROM projects;
        UPDATE SQLITE_SEQUENCE SET SEQ= 0;
    `);
}

test.serial('Should error if startTime is not a valid date', (t) => {
    // eslint-disable-next-line unicorn/consistent-function-scoping
    const test = () =>
        new Time({
            projectName: 'test',
            startTime: 'test',
            tags: [],
        });

    t.throws(test, {
        message: 'Invalid Time parameters',
    });
});

test.serial('Should error if endTime is not a valid date', (t) => {
    // eslint-disable-next-line unicorn/consistent-function-scoping
    const test = () =>
        new Time({
            projectName: 'test',
            startTime: '2020-01-01',
            endTime: 'test',
            tags: [],
        });

    t.throws(test, {
        message: 'Invalid Time parameters',
    });
});

test.serial('Should get the projectID', (t) => {
    const result = new Time({
        projectID: 1,
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [],
    });

    t.is(result.projectID, 1);
});

test.serial('Should get the projectName', (t) => {
    const result = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [],
    });

    t.is(result.projectName, 'test');
});

test.serial('Should get the tags', (t) => {
    const result = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [{ tag: 'test' }],
    });

    t.is(result.tags[0].tag, 'test');
});

test.serial('Should get the startTime', (t) => {
    const result = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [],
    });

    t.is(result.startTime, '2020-01-01T00:00:00.000Z');
});

test.serial('Should get the endTime', (t) => {
    const result = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        endTime: '2020-01-02',
        tags: [],
    });

    t.is(result.endTime, '2020-01-02T00:00:00.000Z');
});

test.serial('Should error if project does not exist', (t) => {
    database.exec(`
        INSERT INTO projects (name) VALUES ('test');
        INSERT INTO projects (name) VALUES ('test 2');
        INSERT INTO tags (tag) VALUES ('test tag');
    `);
    const time = new Time({
        projectName: 'Not Exists',
        startTime: '2020-01-01',
        tags: [],
    });

    const test = () => time.saveTime();

    t.throws(test, {
        message: 'The Not Exists project does not exist',
    });
    clearDB();
});

test.serial('Should throw if the tag does not exist', (t) => {
    database.exec(`
        INSERT INTO projects (name) VALUES ('test');
        INSERT INTO projects (name) VALUES ('test 2');
        INSERT INTO tags (tag) VALUES ('test tag');
    `);
    const time = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [{ tag: 'testTag' }],
    });

    const test = () => time.saveTime();

    t.throws(test, {
        message: 'The testTag tag does not exist',
    });
    clearDB();
});

test.serial('Should save a time without tags', (t) => {
    database.exec(`
        INSERT INTO projects (name) VALUES ('test');
        INSERT INTO projects (name) VALUES ('test 2');
        INSERT INTO tags (tag) VALUES ('test tag');
    `);
    const time = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [],
    });

    time.saveTime();

    const stmt = database.prepare(`
        SELECT * FROM times
        JOIN projects ON projects.id = times.project
    `);
    const result = stmt.get();
    t.is(result.name, 'test');
    t.is(result.start_time, '2020-01-01T00:00:00.000Z');
    clearDB();
});

test.serial('Should save a time with tags', (t) => {
    database.exec(`
        INSERT INTO projects (name) VALUES ('test');
        INSERT INTO projects (name) VALUES ('test 2');
        INSERT INTO tags (tag) VALUES ('test tag');
    `);
    const time = new Time({
        projectName: 'test',
        startTime: '2020-01-01',
        tags: [{ tag: 'test tag' }],
    });

    time.saveTime();

    const stmt = database.prepare(`
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        JOIN projects ON projects.id = project
        JOIN time_tags ON time_tags.time = times.id
        JOIN tags ON tags.id = time_tags.tag
        WHERE
            projects.name = 'test';
    `);
    const result = stmt.get();
    t.is(result.project_name, 'test');
    t.is(result.tag_name, 'test tag');
    clearDB();
});

test.serial('Should update the time details', (t) => {
    database.exec(`
        INSERT INTO projects (name) VALUES ('test');
        INSERT INTO projects (name) VALUES ('test 2');
        INSERT INTO tags (tag) VALUES ('test tag');
    `);
    database.exec(`
        INSERT INTO times (id, start_time, end_time, project) VALUES (99, '2020-01-02', null, 1);
    `);

    new Time({
        id: 99,
        startTime: '2020-01-01',
        endTime: '2020-01-02',
        projectName: 'test',
        tags: [],
    }).saveTime();

    const result = database.prepare('SELECT * from times WHERE id = 99').get();
    t.is(result.end_time, '2020-01-02T00:00:00.000Z');
    clearDB();
});

test.serial('Should get times from a day', (t) => {
    database.exec(`
        INSERT INTO projects
            (id, name)
        VALUES
            (1, 'test'),
            (2, 'test 2');

        INSERT INTO tags
            (id, tag)
        VALUES
            (1, 'test tag'),
            (2, 'test tag 2');

        INSERT INTO times
            (id, start_time, end_time, project)
        VALUES
            (1, '2020-01-01T09:00:00.00',  '2020-01-01T09:10:00.00', 1),
            (2, '2020-01-01T09:10:00.00',  '2020-01-01T09:20:00.00', 1),
            (3, '2020-01-01T09:20:00.00',  null, 2),
            (4, '2020-01-02T09:10:00.00',  '2020-01-02T09:20:00.00', 1);

        INSERT INTO time_tags
            (id, time, tag)
        VALUES
            (1, 1, 1),
            (2, 1, 2);
    `);
    const result = Time.getDay(1, 1, 2020);

    t.is(result.length, 3, 'Wrong number of results');
    t.is(result[0].id, 1, 'Wrong Time ID');
    t.is(result[0].projectName, 'test');
    t.is(result[0].projectID, 1);
    t.is(result[0].tags.length, 2);
    t.is(result[0].tags[0].tag, 'test tag');
    t.is(result[0].tags[1].tag, 'test tag 2');
    t.is(result[0].startTime, '2020-01-01T09:00:00.000Z');
    t.is(result[0].endTime, '2020-01-01T09:10:00.000Z');
    clearDB();
});

test.serial('Should get times from a week', (t) => {
    database.exec(`
        INSERT INTO projects
            (id, name)
        VALUES
            (1, 'test'),
            (2, 'test 2');

        INSERT INTO tags
            (id, tag)
        VALUES
            (1, 'test tag'),
            (2, 'test tag 2');

        INSERT INTO times
            (id, start_time, end_time, project)
        VALUES
            (1, '2020-01-01T09:00:00.00',  '2020-01-01T09:10:00.00', 1),
            (2, '2020-01-01T09:10:00.00',  '2020-01-01T09:20:00.00', 1),
            (3, '2020-01-01T09:20:00.00',  null, 2),
            (4, '2020-01-02T09:10:00.00',  '2020-01-02T09:20:00.00', 1);

        INSERT INTO time_tags
            (id, time, tag)
        VALUES
            (1, 1, 1),
            (2, 1, 2);
    `);
    const result = Time.getWeek(1, 2020);

    t.is(result.length, 4, 'Wrong number of results');
    t.is(result[0].id, 1, 'Wrong Time ID');
    t.is(result[0].projectName, 'test');
    t.is(result[0].projectID, 1);
    t.is(result[0].tags.length, 2);
    t.is(result[0].tags[0].tag, 'test tag');
    t.is(result[0].tags[1].tag, 'test tag 2');
    t.is(result[0].startTime, '2020-01-01T09:00:00.000Z');
    t.is(result[0].endTime, '2020-01-01T09:10:00.000Z');

    clearDB();
});

test.serial('Should get times from a month', (t) => {
    database.exec(`
        INSERT INTO projects
            (id, name)
        VALUES
            (1, 'test'),
            (2, 'test 2');

        INSERT INTO tags
            (id, tag)
        VALUES
            (1, 'test tag'),
            (2, 'test tag 2');

        INSERT INTO times
            (id, start_time, end_time, project)
        VALUES
            (1, '2020-01-01T09:00:00.00',  '2020-01-01T09:10:00.00', 1),
            (2, '2020-01-01T09:10:00.00',  '2020-01-01T09:20:00.00', 1),
            (3, '2020-01-01T09:20:00.00',  null, 2),
            (4, '2020-01-02T09:10:00.00',  '2020-01-02T09:20:00.00', 1);

        INSERT INTO time_tags
            (id, time, tag)
        VALUES
            (1, 1, 1),
            (2, 1, 2);
    `);

    const result = Time.getMonth(1, 2020);

    t.is(result.length, 4, 'Wrong number of results');
    t.is(result[0].id, 1, 'Wrong Time ID');
    t.is(result[0].projectName, 'test');
    t.is(result[0].projectID, 1);
    t.is(result[0].tags.length, 2);
    t.is(result[0].tags[0].tag, 'test tag');
    t.is(result[0].tags[1].tag, 'test tag 2');
    t.is(result[0].startTime, '2020-01-01T09:00:00.000Z');
    t.is(result[0].endTime, '2020-01-01T09:10:00.000Z');

    clearDB();
});

test.serial('Should get times from a year', (t) => {
    database.exec(`
        INSERT INTO projects
            (id, name)
        VALUES
            (1, 'test'),
            (2, 'test 2');

        INSERT INTO tags
            (id, tag)
        VALUES
            (1, 'test tag'),
            (2, 'test tag 2');

        INSERT INTO times
            (id, start_time, end_time, project)
        VALUES
            (1, '2020-01-01T09:00:00.00',  '2020-01-01T09:10:00.00', 1),
            (2, '2020-01-01T09:10:00.00',  '2020-01-01T09:20:00.00', 1),
            (3, '2020-01-01T09:20:00.00',  null, 2),
            (4, '2020-01-02T09:10:00.00',  '2020-01-02T09:20:00.00', 1);

        INSERT INTO time_tags
            (id, time, tag)
        VALUES
            (1, 1, 1),
            (2, 1, 2);
    `);

    const result = Time.getYear(2020);

    t.is(result.length, 4, 'Wrong number of results');
    t.is(result[0].id, 1, 'Wrong Time ID');
    t.is(result[0].projectName, 'test');
    t.is(result[0].projectID, 1);
    t.is(result[0].tags.length, 2);
    t.is(result[0].tags[0].tag, 'test tag');
    t.is(result[0].tags[1].tag, 'test tag 2');
    t.is(result[0].startTime, '2020-01-01T09:00:00.000Z');
    t.is(result[0].endTime, '2020-01-01T09:10:00.000Z');

    clearDB();
});

test.serial('Should get times which are unfinished', (t) => {
    database.exec(`
        INSERT INTO projects
            (id, name)
        VALUES
            (1, 'test'),
            (2, 'test 2');

        INSERT INTO tags
            (id, tag)
        VALUES
            (1, 'test tag'),
            (2, 'test tag 2');

        INSERT INTO times
            (id, start_time, end_time, project)
        VALUES
            (1, '2020-01-01T09:00:00.00',  '2020-01-01T09:10:00.00', 1),
            (2, '2020-01-01T09:10:00.00',  '2020-01-01T09:20:00.00', 1),
            (3, '2020-01-01T09:20:00.00',  null, 2),
            (4, '2020-01-02T09:10:00.00',  '2020-01-02T09:20:00.00', 1);

        INSERT INTO time_tags
            (id, time, tag)
        VALUES
            (1, 1, 1),
            (2, 1, 2);
    `);
    const result = Time.unFinishedTime();

    if (typeof result === 'undefined' || result === null) {
        throw new Error('Result is null');
    } else {
        t.is(result.id, 3, 'Wrong Time ID');
        t.is(result.projectName, 'test 2');
        t.is(result.projectID, 2);
        t.is(result.tags.length, 0);
        t.is(result.startTime, '2020-01-01T09:20:00.000Z');
        // eslint-disable-next-line unicorn/no-null
        t.is(result.endTime, null);
    }

    clearDB();
});
