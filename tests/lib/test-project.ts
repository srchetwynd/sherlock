import test from 'ava';
import { Project } from '../../src/lib/index.js';
import database from '../../src/lib/db/sqlite.js';

function clearDB(): void {
    database.exec(`
        DELETE FROM time_tags;
        DELETE FROM times;
        DELETE FROM tags;
        DELETE FROM projects;
        UPDATE SQLITE_SEQUENCE SET SEQ= 0;
    `);
}

test.serial('Should not error if values are correct', (t) => {
    // eslint-disable-next-line unicorn/consistent-function-scoping
    const test = () => new Project({ name: 'test' });

    t.notThrows(test);
});

test.serial('Should return the id', (t) => {
    const project = new Project({ id: 1, name: 'test' });

    t.is(project.id, 1);
});

test.serial('Should return the name', (t) => {
    const project = new Project({ name: 'test' });

    t.is(project.name, 'test');
});

test.serial('Should save the project', (t) => {
    new Project({ name: 'test' }).saveProject();

    const result = database.prepare('SELECT * FROM projects').all();
    t.is(result.length, 1, 'Wrong length');
    t.is(result[0].id, 1);
    t.is(result[0].name, 'test');
    return clearDB();
});

test.serial('Should get a project', (t) => {
    database.exec(`
        INSERT INTO projects (id, name) VALUES (1, 'test');
        INSERT INTO projects (id, name) VALUES (2, 'test 2');
    `);
    const result = Project.getProject('test');

    if (typeof result === 'undefined' || result === null) {
        throw new Error('Result is null');
    } else {
        t.is(result.id, 1);
        t.is(result.name, 'test');
    }
    clearDB();
});

test.serial('Should return null if the project is not found', (t) => {
    database.exec(`
        INSERT INTO projects (id, name) VALUES (1, 'test');
        INSERT INTO projects (id, name) VALUES (2, 'test 2');
    `);
    const result = Project.getProject('Not Exists');

    t.is(result, undefined);
    clearDB();
});

test.serial('Should return all projects', (t) => {
    database.exec(`
        INSERT INTO projects (id, name) VALUES (1, 'test');
        INSERT INTO projects (id, name) VALUES (2, 'test 2');
    `);
    const result = Project.getAllProjects();

    t.truthy(Array.isArray(result));
    t.is(result[0].id, 1);
    t.is(result[0].name, 'test');
    t.is(result[1].id, 2);
    t.is(result[1].name, 'test 2');
    clearDB();
});
