import test from 'ava';
import { Tag } from '../../src/lib/index.js';
import database from '../../src/lib/db/sqlite.js';

function clearDB(): void {
    database.exec(`
        DELETE FROM time_tags;
        DELETE FROM times;
        DELETE FROM tags;
        DELETE FROM projects;
        UPDATE SQLITE_SEQUENCE SET SEQ= 0;
    `);
}

test.serial('Should not error if tag is correct', (t) => {
    // eslint-disable-next-line unicorn/consistent-function-scoping
    const test = () => new Tag({ tag: 'test' });

    t.notThrows(test);
});

test.serial('Should return the tag name', (t) => {
    const tag = new Tag({ tag: 'test' });

    t.is(tag.tag, 'test');
});

test.serial('Should return the tag id', (t) => {
    const tag = new Tag({ id: 1, tag: 'test' });

    t.is(tag.id, 1);
});

test.serial('Should save a tag', (t) => {
    const tag = new Tag({ tag: 'test' });
    tag.saveTag();

    const result = database.prepare('SELECT * FROM tags').all();
    t.is(result.length, 1);
    t.is(result[0].tag, 'test');
    clearDB();
});

test.serial('Should return a tag', (t) => {
    database.exec(`
        INSERT INTO tags (id, tag) VALUES (1, 'test');
        INSERT INTO tags (id, tag) VALUES (2, 'test 2');
    `);
    const result = Tag.getTag('test');

    if (typeof result === 'undefined' || result === null) {
        throw new Error('Result is null');
    } else {
        t.is(result.tag, 'test');
        t.is(result.id, 1);
    }
    clearDB();
});

test.serial('Should return null if no matching tag', (t) => {
    database.exec(`
        INSERT INTO tags (id, tag) VALUES (1, 'test');
        INSERT INTO tags (id, tag) VALUES (2, 'test 2');
    `);
    const result = Tag.getTag('Not Exists');

    t.is(result, undefined);
    clearDB();
});

test.serial('Should return an array of all tags', (t) => {
    database.exec(`
        INSERT INTO tags (id, tag) VALUES (1, 'test');
        INSERT INTO tags (id, tag) VALUES (2, 'test 2');
    `);
    const result = Tag.getAllTags();

    t.true(Array.isArray(result));
    t.is(result[0].id, 1);
    t.is(result[0].tag, 'test');
    t.is(result[1].id, 2);
    t.is(result[1].tag, 'test 2');
    clearDB();
});
