import database from './db/index.js';
import ProjectLocal from './project.js';
import TagLocal from './tag.js';
import TimeLocal from './time.js';

TagLocal._db = database;
ProjectLocal._db = database;
TimeLocal._db = database;
TimeLocal._Tag = TagLocal;
TimeLocal._Project = ProjectLocal;

export const Project = ProjectLocal;
export const Tag = TagLocal;
export const Time = TimeLocal;
