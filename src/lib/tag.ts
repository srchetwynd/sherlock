import { TagDB, RawTag, Tag as TagInterface } from '../types';
import SError from './sherlock-error.js';

/**
 * @class Tag
 * @description Tag holds details about a tag
 * @property tag - @readonly the name of the tag
 * @property id - the id of the tag
 *
 * @param options - options object
 * @param [options.id] - the id of the tag
 * @param options.tag - the name of the tag
 */
class Tag implements TagInterface {
    private _id: number | undefined;
    private _tag: string;

    constructor({ id, tag }: RawTag) {
        const errors = [];
        if (typeof tag !== 'string') {
            errors.push(`Tag must be of type string got: ${typeof tag}`);
        }
        if (errors.length > 0) {
            new SError('Tag validation failed', errors);
        }

        this._tag = tag;
        this._id = id;
    }

    get tag(): string {
        return this._tag;
    }

    get id(): number | undefined {
        return this._id;
    }

    set id(id: number | undefined) {
        this._id = id;
    }

    /**
     * @function saveTag
     * @description saves a tag
     * @memberof Tag
     * @instance
     * @returns - the tag
     */
    saveTag(): Tag {
        const tagJson = Tag._db.saveTag(this);
        this._id = tagJson.lastInsertRowid;
        return this;
    }

    /**
     * @function getTag
     * @description gets a tag given its name
     * @memberof Tag
     * @static
     * @param tag - the name of the tag to get
     * @returns - the tag or null if the tag does not exist
     */
    static getTag(tag: string): Tag | void {
        const tagJson = Tag._db.getTag(tag);

        if (tagJson) {
            return new Tag(tagJson);
        }
    }

    /**
     * @function getAllTags
     * @description gets a list of all tags
     * @memberof Tag
     * @static
     * @returns - Array of tags
     */
    static getAllTags(): Tag[] {
        const tagJson = Tag._db.getAllTags();

        return tagJson.map((element) => new Tag(element));
    }

    static _db: TagDB;
}

export default Tag;
