import Database from 'better-sqlite3';
import os from 'node:os';
import fs from 'node:fs';

const databasePath = os.homedir() + '/.config/sherlock';

if (fs.existsSync(databasePath) === false) {
    fs.mkdirSync(databasePath, { recursive: true });
}

const database: Database.Database = new Database(databasePath + '/sherlock.db');

database.exec(`
    PRAGMA foreign_keys = ON;

    CREATE TABLE IF NOT EXISTS projects (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name VARCHAR NOT NULL
    );

    CREATE TABLE IF NOT EXISTS tags (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        tag VARCHAR NOT NULL
    );

    CREATE TABLE IF NOT EXISTS times (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        start_time DATETIME NOT NULL DEFAULT NOW,
        end_time DATETIME,
        project INTEGER NOT NULL,
        FOREIGN KEY (project) REFERENCES projects(id) ON DELETE CASCADE
    );

    CREATE TABLE IF NOT EXISTS time_tags (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        time INTEGER NOT NULL,
        tag INTEGER NOT NULL,
        FOREIGN KEY (time) REFERENCES times(id) ON DELETE CASCADE,
        FOREIGN KEY (tag) REFERENCES tags(id) ON DELETE CASCADE
    );
`);

export default database;
