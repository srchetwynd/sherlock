import { RawProject, RunResult } from '../../types';
import database from './sqlite.js';

/**
 * @function getProject
 * @description gets a project by its name
 * @memberof db
 * @param name - the name of the project to get
 * @returns - the project
 */
function getProject(name: string): RawProject {
    const stmt = database.prepare(`
        SELECT * FROM projects WHERE name = ?
    `);
    return stmt.get(name);
}

/**
 * @function saveProject
 * @description saves a new project
 * @memberof db
 * @param  project - the name of the new project
 * @returns - contains details about the completed query
 */
function saveProject(project: RawProject): RunResult {
    const stmt = database.prepare(`
        INSERT INTO projects (name) VALUES (?)
    `);
    return stmt.run(project.name);
}

/**
 * @function getAllProjects
 * @description gets all projects
 * @memberof db
 * @returns - array of projects
 */
function getAllProjects(): RawProject[] {
    const stmt = database.prepare(`
        SELECT * FROM projects
    `);
    return stmt.all();
}

export default {
    getProject,
    saveProject,
    getAllProjects,
};
