import { RawTag, RunResult } from '../../types';
import database from './sqlite.js';

/**
 * @function getTag
 * @description gets a tag by its name
 * @memberof db
 * @param  name - the name of the tag
 * @returns - the tag
 */
function getTag(name: string): RawTag {
    const stmt = database.prepare(`
        SELECT * FROM tags WHERE tag = ?
    `);
    return stmt.get(name);
}

/**
 * @function saveTag
 * @description saves a new tag
 * @memberof db
 * @param tag - the name of the new tag
 * @returns details about the tag
 */
function saveTag(tag: RawTag): RunResult {
    const stmt = database.prepare(`
        INSERT INTO tags (tag) VALUES (?)
    `);
    return stmt.run(tag.tag);
}

/**
 * @function getAllTags
 * @description gets all the tags
 * @memberof db
 * @returns - array of tag objects
 */
function getAllTags(): RawTag[] {
    const stmt = database.prepare(`
        SELECT * FROM tags
    `);
    return stmt.all();
}

export default {
    getTag,
    saveTag,
    getAllTags,
};
