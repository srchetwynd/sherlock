import project from './project.js';
import tag from './tag.js';
import time from './time.js';

/**
 * @namespace db
 */

export default {
    ...project,
    ...tag,
    ...time,
};
