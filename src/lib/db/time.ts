import { RawTime, Time } from '../../types';
import database from './sqlite.js';

interface TimeDB {
    start_time: string;
    end_time: string;
    time_id: number;
    project_name: string;
    project_id: number;
    tag_name: string;
    tag_id: number;
}

/**
 * @function getATime
 * @description gets a single time
 * @param [id] - the id of the time, if not given will get the last time
 * @returns - time object
 */
function getATime(id?: number): RawTime {
    let sql = `
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        LEFT JOIN projects ON projects.id = project
        LEFT JOIN time_tags ON time_tags.time = times.id
        LEFT JOIN tags ON tags.id = time_tags.tag
    `;

    if (id) {
        sql += 'WHERE times.id = ?';
        return serializeResults(database.prepare(sql).all(id))[0];
    } else {
        const id = database
            .prepare(sql + 'ORDER BY times.id DESC')
            .get().time_id;
        return serializeResults(
            database.prepare(sql + 'WHERE times.id = ?').all(id)
        )[0];
    }
}

/**
 * @function updateTime
 * @description updates an existing time
 * @memberof db
 * @param time - the time to save
 * @returns - no return value
 */
function updateTime(time: Time): void {
    const timeStmt = database.prepare(`
        UPDATE times SET
            start_time = ?,
            end_time = ?,
            project = ?
        WHERE id = ?
    `);
    const deleteTagStmt = database.prepare(`
        DELETE FROM time_tags WHERE time = ?
    `);
    const tagProjectStmt = database.prepare(`
        INSERT INTO time_tags (time, tag) VALUES (?, ?)
    `);
    database.transaction(() => {
        deleteTagStmt.run(time.id);
        timeStmt.run(time.startTime, time.endTime, time.projectID, time.id);
        for (const tag of time.tags) {
            tagProjectStmt.run(time.id, tag.id);
        }
    })();
}

/**
 * @function saveTime
 * @description saves a new time
 * @memberof db
 * @param time - the time to update
 * @returns - no return value
 */
function saveTime(time: Time): void {
    const timeStmt = database.prepare(`
        INSERT INTO times (start_time, end_time, project) VALUES (?, ?, ?);
    `);
    const tagProjectStmt = database.prepare(`
        INSERT INTO time_tags (time, tag) VALUES (?, ?)
    `);

    database.transaction(() => {
        const insertResult = timeStmt.run(
            time.startTime,
            time.endTime,
            time.projectID
        );

        for (const tag of time.tags) {
            tagProjectStmt.run(insertResult.lastInsertRowid, tag.id);
        }
    })();
}

/**
 * @function unFinishedTime
 * @description gets any unfinished times
 * @memberof db
 * @returns - the time object
 */
function unFinishedTime(): RawTime {
    const stmt = database.prepare(`
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        LEFT JOIN projects ON projects.id = project
        LEFT JOIN time_tags ON time_tags.time = times.id
        LEFT JOIN tags ON tags.id = time_tags.tag
        WHERE
            end_time is NULL;
    `);
    const result = stmt.all();

    return serializeResults(result)[0];
}

/**
 * @function getDay
 * @description gets all time objects for a day
 * @memberof db
 * @param date - the date
 * @param month - the month
 * @param year - the year
 * @returns - times
 */
function getDay(date: number, month: number, year: number): RawTime[] {
    const dateString = String(date).padStart(2, '0');
    const monthString = String(month).padStart(2, '0');
    const yearString = String(year);
    const stmt = database.prepare(`
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        LEFT JOIN projects ON projects.id = project
        LEFT JOIN time_tags ON time_tags.time = times.id
        LEFT JOIN tags ON tags.id = time_tags.tag
        WHERE
            strftime('%d', times.start_time) = ? AND
            strftime('%m', times.start_time) = ? AND
            strftime('%Y', times.start_time) = ?;
    `);
    const result = stmt.all(dateString, monthString, yearString);

    return serializeResults(result);
}

/**
 * @function getWee
 * @description gets all time objects for a week
 * @memberof db
 * @param week - the week
 * @param year - the year
 * @returns - times
 */
function getWeek(week: number, year: number): RawTime[] {
    const weekString = String(week - 1).padStart(2, '0');
    const yearString = String(year);
    const stmt = database.prepare(`
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        LEFT JOIN projects ON projects.id = project
        LEFT JOIN time_tags ON time_tags.time = times.id
        LEFT JOIN tags ON tags.id = time_tags.tag
        WHERE
            strftime('%W', times.start_time) = ? AND
            strftime('%Y', times.start_time) = ?;
    `);
    const result = stmt.all(weekString, yearString);

    return serializeResults(result);
}

/**
 * @function getMonth
 * @description gets all time objects for a month
 * @memberof db
 * @param month - the month
 * @param year - the year
 * @returns - times
 */
function getMonth(month: number, year: number): RawTime[] {
    const monthString = String(month).padStart(2, '0');
    const yearString = String(year);
    const stmt = database.prepare(`
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        LEFT JOIN projects ON projects.id = project
        LEFT JOIN time_tags ON time_tags.time = times.id
        LEFT JOIN tags ON tags.id = time_tags.tag
        WHERE
            strftime('%m', times.start_time) = ? AND
            strftime('%Y', times.start_time) = ?
        ORDER BY start_time ASC;
    `);
    const result = stmt.all(monthString, yearString);

    return serializeResults(result);
}

/**
 * @function getYear
 * @description gets all time objects for a year
 * @memberof db
 * @param year - the year
 * @returns - times
 */
function getYear(year: number): RawTime[] {
    const yearString = String(year);
    const stmt = database.prepare(`
        SELECT
            times.start_time,
            times.end_time,
            times.id as time_id,
            projects.name AS project_name,
            projects.id AS project_id,
            tags.tag AS tag_name,
            tags.id AS tag_id
        FROM
            times 
        LEFT JOIN projects ON projects.id = project
        LEFT JOIN time_tags ON time_tags.time = times.id
        LEFT JOIN tags ON tags.id = time_tags.tag
        WHERE
            strftime('%Y', times.start_time) = ?;
    `);
    const result = stmt.all(yearString);

    return serializeResults(result);
}

/**
 * @function serializeResults
 * @description converts the db results into something useable in Time
 * @memberof db
 * @private
 * @param timeJson - db result
 * @returns - formated results
 */
function serializeResults(timeJson: TimeDB[]): RawTime[] {
    const timeMap: { [key: number]: RawTime } = {};
    for (const time of timeJson) {
        if (!timeMap[time.time_id]) {
            timeMap[time.time_id] = {
                id: time.time_id,
                projectID: time.project_id,
                projectName: time.project_name,
                tags: [],
                startTime: time.start_time,
                endTime: time.end_time,
            };
        }
        if (time.tag_name) {
            timeMap[time.time_id].tags.push({
                id: time.tag_id,
                tag: time.tag_name,
            });
        }
    }
    return Object.values(timeMap).sort((a, b) => {
        if (new Date(a.startTime) === new Date(b.startTime)) {
            return 0;
        } else if (new Date(a.startTime) > new Date(b.startTime)) {
            return 1;
        } else {
            return -1;
        }
    });
}

export default {
    getATime,
    updateTime,
    saveTime,
    getDay,
    getWeek,
    getMonth,
    getYear,
    unFinishedTime,
};
