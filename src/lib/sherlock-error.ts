class SError extends Error {
    details: string[] | undefined;
    constructor(message: string, details?: string[]) {
        super(message);
        this.details = details;
    }
}

export default SError;
