import { TimeDB, RawTime, ProjectInterface, Tag, TagInterface } from '../types';
import SError from './sherlock-error.js';
/**
 * @class Time
 * @description a Time class which contains details about the time
 * @property propertyID - @readonly the id of the project the time is recorded against
 * @property projectName - @readonly the name of the project the time is recorded against
 * @property tags - @readonly the tags the time is recorded with
 * @property startTime - @readonly ISO8601 date string for when the recording was started
 * @property endTime - ISO8601 date string for when the recording was ended
 * @property id - @readonly the id of the recording
 *
 * @param options - options object
 * @param [options.id] - the id of the time
 * @param [options.projectID] - the id of the project
 * @param options.projectName - the name of the project
 * @param options.tags - array of tag names
 * @param options.startTime - ISO8601 date string for when the recoding was started
 * @param [options.endTime] - ISO8601 date string for when the recoding ended
 */
class Time {
    private _id: number | undefined;
    private _projectID: number | undefined;
    private _projectName: string;
    private _tags: Tag[];
    private _startTime: string;
    private _endTime: string | null;

    constructor({
        id,
        projectID,
        projectName,
        tags,
        startTime,
        endTime,
    }: RawTime) {
        const errors = [];
        // eslint-disable-next-line unicorn/no-null
        this._endTime = null;
        this._startTime = '';

        if (typeof projectName !== 'string') {
            errors.push(
                `projectName must be a string, got: ${typeof projectName}`
            );
        }
        if (new Date(startTime).toString() === 'Invalid Date') {
            errors.push(`startTime must be a valid date, got: ${startTime}`);
        } else {
            this._startTime = new Date(startTime).toISOString();
        }
        if (
            typeof endTime !== 'undefined' &&
            endTime !== null &&
            new Date(endTime).toString() === 'Invalid Date'
        ) {
            errors.push(
                `endTime, if provided, must be a valid date, got: ${endTime}`
            );
        } else if (typeof endTime !== 'undefined' && endTime !== null) {
            this._endTime = new Date(endTime).toISOString();
        } else {
            // eslint-disable-next-line unicorn/no-null
            this._endTime = null;
        }

        if (!Array.isArray(tags)) {
            errors.push(`tags must be an array, got: ${typeof tags}`);
        }
        if (errors.length > 0) {
            throw new SError('Invalid Time parameters', errors);
        }

        this._id = id;
        this._projectID = projectID;
        this._projectName = projectName;
        this._tags = tags.map((element) => new Time._Tag(element));
    }

    get projectID(): number | undefined {
        return this._projectID;
    }

    get projectName(): string {
        return this._projectName;
    }

    get tags(): Tag[] {
        return this._tags;
    }

    get startTime(): string {
        return this._startTime;
    }

    get endTime(): string | null {
        return this._endTime;
    }

    set endTime(time: string | null) {
        this._endTime = time;
    }

    get id(): number | undefined {
        return this._id;
    }

    /**
     * @function saveTime
     * @description if the time has no id it will create a new record for the time otherwise it will update an existing time
     * @memberof Time
     * @instance
     * @returns - no return value
     */
    saveTime(): void {
        const timeJson = Time._Project.getProject(this._projectName);
        if (typeof timeJson === 'undefined' || timeJson === null) {
            throw new Error(`The ${this._projectName} project does not exist`);
        }

        if (typeof timeJson.id === 'number') {
            this._projectID = timeJson.id;
        }

        for (const element of this._tags) {
            const tagJson = Time._Tag.getTag(element.tag);
            if (typeof tagJson === 'undefined' || tagJson === null) {
                throw new Error(`The ${element.tag} tag does not exist`);
            }
            element.id = tagJson.id;
        }

        if (this._id) {
            Time._db.updateTime(this);
        } else {
            Time._db.saveTime(this);
        }
    }

    toString(): string {
        return JSON.stringify(
            {
                projectName: this.projectName,
                tags: this.tags.map((element) => element.tag),
                startTime: new Date(this.startTime).toString(),
                endTime:
                    this.endTime === null
                        ? undefined
                        : new Date(this.endTime).toString(),
            },
            undefined,
            4
        );
    }

    /**
     * @function getDay
     * @description gets all times from a day
     * @memberof Time
     * @static
     * @param date - the date
     * @param month - the month
     * @param year - the year
     * @returns - array of times from the given date
     */
    static getDay(date: number, month: number, year: number): Time[] {
        const timeJson = Time._db.getDay(date, month, year);

        return timeJson.map((element) => new Time(element));
    }

    /**
     * @function getWeek
     * @description gets all times from a week
     * @memberof Time
     * @static
     * @param week - the week
     * @param year - the year
     * @returns - array of times from the given week
     */
    static getWeek(week: number, year: number): Time[] {
        const timeJson = Time._db.getWeek(week, year);

        return timeJson.map((element) => new Time(element));
    }

    /**
     * @function getMonth
     * @description gets all times from a wee
     * @memberof Time
     * @static
     * @param month - the month
     * @param year - the year
     * @returns - array of times from the given month
     */
    static getMonth(month: number, year: number): Time[] {
        const timeJson = Time._db.getMonth(month, year);

        return timeJson.map((element) => new Time(element));
    }

    /**
     * @function getYear
     * @description gets all times from a year
     * @memberof Time
     * @static
     * @param year - the year
     * @returns - array of times from the given year
     */
    static getYear(year: number): Time[] {
        const timeJson = Time._db.getYear(year);

        return timeJson.map((element) => new Time(element));
    }

    /**
     * @function unFinishedTime
     * @description gets the currently unfinished time
     * @memberof Time
     * @static
     * @returns - the unfinished time
     */
    static unFinishedTime(): Time | void {
        const timeJson = Time._db.unFinishedTime();

        if (timeJson) {
            return new Time(timeJson);
        }
    }

    /**
     * @function getATime
     * @description gets a single time record by its id, if no id returns the last time
     * @memberof Time
     * @param [id] - the id of the time record
     * @static
     * @returns - the time record
     */
    static getATime(id: number): Time {
        const timeJson = Time._db.getATime(id);

        return new Time(timeJson);
    }

    static _db: TimeDB;

    static _Tag: TagInterface;

    static _Project: ProjectInterface;
}

export default Time;
