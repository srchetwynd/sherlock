import { RawProject, ProjectDB } from '../types';
import SError from './sherlock-error.js';

/**
 * @class Project
 * @description A Project class contains the details of a project
 * @property {number} id - @readonly the id of the project if there is one
 * @property {string} name - @readonly the name of the project
 *
 * @param {object} options - an object containing options
 * @param {number} [options.id] - the id of the project
 * @param {string} options.name - the name of the project
 */
class Project {
    private _id: number | undefined;
    private _name: string;
    constructor({ id, name }: RawProject) {
        const errors = [];
        if (typeof name !== 'string') {
            errors.push(`Name must be of type string got: ${typeof name}`);
        }
        if (errors.length > 0) {
            throw new SError('Project validation failed', errors);
        }

        this._name = name;
        this._id = id;
    }

    get id(): number | undefined {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    /**
     * @function saveProject
     * @description saves the project to the db
     * @memberof Project
     * @instance
     * @returns - this project
     */
    saveProject(): Project {
        const projectJson = Project._db.saveProject(this);
        this._id = projectJson.lastInsertRowid;
        return this;
    }

    /**
     * @function getProject
     * @description gets a project by its name
     * @memberof Project
     * @static
     * @param name - the name of the project to get
     * @returns The project or null if it does not exist
     */
    static getProject(name: string): Project | void {
        const projectJson: RawProject = Project._db.getProject(name);

        if (projectJson) {
            return new Project(projectJson);
        }
    }

    /**
     * @function getAllProjects
     * @description gets all the projects
     * @memberof Project
     * @static
     * @returns - array of projects
     */
    static getAllProjects(): Project[] {
        const projectJson: RawProject[] = Project._db.getAllProjects();

        return projectJson.map((element: RawProject) => new Project(element));
    }

    static _db: ProjectDB;
}

export default Project;
