export interface RawProject {
    id?: number;
    name: string;
}

export interface RawTag {
    id?: number;
    tag: string;
}

export interface RawTime {
    id?: number;
    projectID?: number;
    projectName: string;
    tags: RawTag[];
    startTime: string;
    endTime?: string;
}

export interface RunResult {
    changes: number;
    lastInsertRowid: Integer.IntLike;
}

export interface ProjectDB {
    getProject: (name: string) => RawProject;
    saveProject: (project: RawProject) => RunResult;
    getAllProjects: () => RawProject[];
}

export interface TagDB {
    getTag: (name: string) => RawTag;
    saveTag: (tag: RawTag) => RunResult;
    getAllTags: () => RawTag[];
}

export interface TimeDB {
    getATime: (id: number) => RawTime;
    updateTime: (Time) => void;
    saveTime: (Time) => void;
    getDay: (date: number, month: number, year: number) => RawTime[];
    getWeek: (week: number, year: number) => RawTime[];
    getMonth: (month: number, year: number) => RawTime[];
    getYear: (year: number) => RawTime[];
    unFinishedTime: (id?: number) => RawTime | undefined;
}

export interface Project {
    id?: number;
    name: string;
    saveProject(): Project;
}

export interface ProjectInterface {
    new ({ id, name }: RawProject);
    static getProject(name: string): Project | void;
    static getAllProjects(): Project[];
    _db: ProjectDB;
}

export interface Tag {
    tag: string;
    id?: number;
    saveTag(): Tag;
}

export interface TagInterface {
    new ({ id, tag }: RawTag);
    static getTag(tag: string): Tag | void;
    static getAllTags(): Tag[];
    _db: TagDB;
}

export interface TagStatic {
    getTag(tag: string): Tag | null;
    getAllTags(): Tag[];
    _db: TagDB;
}

export interface Time {
    projectID?: number;
    projectName: string;
    tags: Tag[];
    startTime: string;
    endTime: string | null;
    id?: number;
    saveTime(): void;
    toString(): string;
}

export interface TimeInterface {
    new ({ id, projectID, projectName, tags, startTime, endTime }: RawTime);
    static getDay(date: number, month: number, year: number): Time[];
    static getWeek(week: number, year: number): Time[];
    static getMonth(month: number, year: number): Time[];
    static getYear(year: number): Time[];
    static unFinishedTime(): Time | null;
    static getATime(id: number): Time;
    _db: TimeDB;
    _Tag: Tag;
    _Project: Project;
}
