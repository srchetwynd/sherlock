import { Command } from 'commander';
import { Project, Tag, Time } from '../lib/index.js';
import helpers from './helpers.js';
import fs from 'node:fs';
import { spawnSync } from 'node:child_process';
import { LIB_VERSION } from '../version.js';
const program = new Command();
const editor = process.env.EDITOR ?? 'vi';

program.version(LIB_VERSION);

program
    .command('new-project <project_name>')
    .description('Adds a new project to start recording time against')
    .action((project_name: string) => {
        new Project({
            name: project_name,
            id: undefined,
        }).saveProject();
    });

program
    .command('projects')
    .description('Lists all projects')
    .action(() => {
        const projects = Project.getAllProjects();
        console.log(process.env);
        if (projects.length > 0) {
            helpers.outputter(
                '\n\tYou have these projects available to record against:'
            );
            for (const element of projects) {
                helpers.outputter('\t -', element.name);
            }
        } else {
            helpers.outputter(
                '\n\tYou currently have no projects to record against'
            );
        }
    });

program
    .command('new-tag <tag_name>')
    .description('Adds a new tag to record time against')
    .action((tag_name: string) => {
        new Tag({
            tag: tag_name,
            id: undefined,
        }).saveTag();
    });

program
    .command('tags')
    .description('Lists all tags')
    .action(() => {
        const tags = Tag.getAllTags();
        if (tags.length > 0) {
            helpers.outputter(
                '\n\tYou have these tags available to record against:'
            );
            for (const element of tags) {
                helpers.outputter('\t -', element.tag);
            }
        } else {
            helpers.outputter(
                '\n\tYou currently have no tags to record against'
            );
        }
    });

program
    .command('start <project> [tags...]')
    .description(
        'Starts recording time against a project and tags, stops any currently recording times'
    )
    .action((project: string, tags: string[]) => {
        const current = Time.unFinishedTime();
        if (typeof current !== 'undefined' && current !== null) {
            current.endTime = new Date().toISOString();
            current.saveTime();
            helpers.outputter(
                `\n\t${helpers.RED}Stopped recording${helpers.CLEAR}`
            );
            helpers.printTime(current);
        }

        try {
            new Time({
                projectName: project,
                startTime: new Date().toISOString(),
                tags: tags.map((element) => ({ id: undefined, tag: element })),
            }).saveTime();
            helpers.outputter(
                `\n\t${helpers.GREEN}Started recording${helpers.CLEAR}`,
                project
            );
        } catch (error) {
            helpers.outputter(
                `\n\t${helpers.RED}Error starting recording:${helpers.CLEAR}`,
                '\n\t -',
                (error as Error).message
            );
        }
    });

program
    .command('stop')
    .description('Stops any currently recording times')
    .action(() => {
        const current = Time.unFinishedTime();
        if (current) {
            current.endTime = new Date().toISOString();
            current.saveTime();
            helpers.printTime(current);
        } else {
            helpers.outputter('\n\tYou are not currently recording a time');
        }
    });

program
    .command('status')
    .description('Shows the status of the currently recording time')
    .action(() => {
        const current = Time.unFinishedTime();
        if (current) {
            helpers.printTime(current);
        } else {
            helpers.outputter('\n\tYou are not currently recording a time');
        }
    });

program
    .command('day [date]')
    .description('Lists all the Times for the given date')
    .option('-j, --json', 'Prints the times as json')
    .option(
        '-t --tags',
        'Groups the times by tag under the total time for the project'
    )
    .action((date: string, options) => {
        const parsedDate = date ? new Date(date) : new Date();
        const times = Time.getDay(
            parsedDate.getDate(),
            parsedDate.getMonth() + 1,
            parsedDate.getFullYear()
        );
        if (options.json) {
            for (const element of times) {
                helpers.outputter(
                    JSON.stringify(
                        {
                            id: element.id,
                            projectName: element.projectName,
                            tags: element.tags.map((element_) => element_.tag),
                            startTime: new Date(element.startTime).toString(),
                            endTime:
                                element.endTime === null
                                    ? undefined
                                    : new Date(element.endTime).toString(),
                        },
                        undefined,
                        4
                    )
                );
            }
        } else {
            helpers.outputter(
                '\n\t\t',
                helpers.BOLD_UNDERLINE,
                helpers.formatDate(parsedDate),
                helpers.CLEAR
            );
            if (times.length === 0) {
                helpers.outputter('\n\tThere are no recordings for this date');
            }
            for (const element of helpers.groupByProject(times)) {
                helpers.outputter(
                    '\t -',
                    element[0].projectName,
                    '\t\t',
                    helpers.formatDuration(helpers.totalDuration(element))
                );
                if (options.tags) {
                    for (const [tag, time] of Object.entries(
                        helpers.groupByTag(element)
                    )) {
                        helpers.outputter(
                            '\t --',
                            tag,
                            '\t\t',
                            helpers.formatDuration(helpers.totalDuration(time))
                        );
                    }
                }
            }
            helpers.outputter(
                '\n\t - Total\t\t',
                helpers.formatDuration(helpers.totalDuration(times))
            );
            helpers.outputter('\t - Context Switches\t', times.length);
        }
    });

program
    .command('week [week]')
    .description('Lists all the Times for the given week')
    .option('-j, --json', 'Prints the times as json')
    .option('-y, --year <year>', 'The year of the week to show recordings for')
    .option(
        '-t --tags',
        'Groups the times by tag under the total time for the project'
    )
    .action((week: string, options) => {
        const weekString = week ?? helpers.getWeek();
        const yearString = options.year ?? new Date().getFullYear();

        const times = Time.getWeek(Number(weekString), Number(yearString));
        if (options.json) {
            for (const element of times) {
                helpers.outputter(
                    JSON.stringify(
                        {
                            id: element.id,
                            projectName: element.projectName,
                            tags: element.tags.map((element_) => element_.tag),
                            startTime: new Date(element.startTime).toString(),
                            endTime:
                                element.endTime === null
                                    ? undefined
                                    : new Date(element.endTime).toString(),
                        },
                        undefined,
                        4
                    )
                );
            }
        } else {
            helpers.outputter(
                '\n\t\t',
                helpers.BOLD_UNDERLINE,
                'mWeek:',
                weekString,
                'Year:',
                yearString,
                helpers.CLEAR
            );
            for (const element of helpers.groupByDate(times)) {
                const date = new Date(element[0].startTime);
                helpers.outputter(
                    '\n\t\t',
                    helpers.BOLD_UNDERLINE,
                    helpers.formatDate(date),
                    helpers.CLEAR
                );
                for (const element_ of helpers.groupByProject(element)) {
                    helpers.outputter(
                        '\t -',
                        element_[0].projectName,
                        '\t\t',
                        helpers.formatDuration(helpers.totalDuration(element_))
                    );
                    if (options.tags) {
                        for (const [tag, time] of Object.entries(
                            helpers.groupByTag(element_)
                        )) {
                            helpers.outputter(
                                '\t --',
                                tag,
                                '\t\t',
                                helpers.formatDuration(
                                    helpers.totalDuration(time)
                                )
                            );
                        }
                    }
                }
                helpers.outputter(
                    '\t - Total',
                    '\t\t',
                    helpers.formatDuration(helpers.totalDuration(element))
                );
            }
        }
    });

program
    .command('month [month]')
    .description('Lists all the Times for the given month')
    .option('-y, --year <year>', 'The year of the month to show recordings for')
    .option(
        '-t --tags',
        'Groups the times by tag under the total time for the project'
    )
    .action((month: string, options) => {
        const monthString = month
            ? Number.parseInt(month)
            : new Date().getMonth() + 1;
        const yearString = options.year ?? new Date().getFullYear();
        const times = Time.getMonth(monthString, yearString);
        helpers.outputter(
            '\n\t\t',
            helpers.BOLD_UNDERLINE,
            'Month:',
            String(monthString),
            'Year:',
            String(yearString),
            helpers.CLEAR
        );
        for (const element of helpers.groupByDate(times)) {
            const date = new Date(element[0].startTime);
            helpers.outputter(
                '\n\t\t',
                helpers.BOLD_UNDERLINE,
                helpers.formatDate(date),
                helpers.CLEAR
            );
            for (const element_ of helpers.groupByProject(element)) {
                helpers.outputter(
                    '\t -',
                    element_[0].projectName,
                    '\t\t',
                    helpers.formatDuration(helpers.totalDuration(element_))
                );
                if (options.tags) {
                    for (const [tag, time] of Object.entries(
                        helpers.groupByTag(element_)
                    )) {
                        helpers.outputter(
                            '\t --',
                            tag,
                            '\t\t',
                            helpers.formatDuration(helpers.totalDuration(time))
                        );
                    }
                }
            }
            helpers.outputter(
                '\t - Total',
                '\t\t',
                helpers.formatDuration(helpers.totalDuration(element))
            );
        }
    });

program
    .command('year [year]')
    .description('Lists all the Times for the given year')
    .action((year: string) => {
        const yearString = String(year ?? new Date().getFullYear());
        const times = Time.getYear(Number(yearString));
        helpers.outputter(
            '\n\t\t',
            helpers.BOLD_UNDERLINE,
            'Year:',
            yearString,
            helpers.CLEAR
        );
        for (const element of helpers.groupByProject(times)) {
            helpers.outputter(
                '\t -',
                element[0].projectName,
                '\t\t',
                helpers.formatDuration(helpers.totalDuration(element))
            );
        }
        helpers.outputter(
            '\t - Total',
            '\t\t',
            helpers.formatDuration(helpers.totalDuration(times))
        );
    });

program
    .command('edit [id]')
    .description('Edits the Time')
    .action((id: string) => {
        const time = Time.getATime(Number(id));
        try {
            fs.writeFileSync('/tmp/json', time.toString());
            spawnSync(editor, ['/tmp/json'], {
                stdio: 'inherit',
            });
        } catch (error) {
            helpers.outputter(
                '\n\t',
                helpers.RED,
                'Error retrieving recording:',
                helpers.CLEAR,
                '\n\t -',
                (error as Error).message
            );
            return;
        }

        try {
            const file = fs.readFileSync('/tmp/json').toString();
            const parsed = JSON.parse(file);
            parsed.tags = parsed.tags.map((tag: string) => ({ tag }));
            const newTime = new Time({
                ...parsed,
                id: time.id,
            });
            newTime.saveTime();
        } catch (error) {
            helpers.outputter(
                '\n\t',
                helpers.RED,
                'Error saving recording:',
                helpers.CLEAR,
                '\n\t -',
                (error as Error).message
            );
            return;
        }
    });

program.parse(process.argv);
