import { Time, Tag } from '../types';

const RED = '\u001B[31m';
const GREEN = '\u001B[32m';
const ORANGE = '\u001B[33m';
const DARK_BLUE = '\u001B[34m';
const BLUE = '\u001B[35m';
const CLEAR = '\u001B[0m';
const BOLD_UNDERLINE = '\u001B[4;37m';

function outputter(...strings: (string | number)[]): void {
    // eslint-disable-next-line no-console
    console.log(...strings);
}

/**
 * @function getDuration
 * @description pretty formats the time between two dates
 * @memberof helpers
 * @param date1 - first date
 * @param date2 - second date
 * @returns - prettily formated date string
 */
function getDuration(date1: Date, date2: Date): string {
    const distance = Math.abs(date1.getTime() - date2.getTime());
    return formatDuration(distance);
}

/**
 * @function formatDuration
 * @description given a duration in miliseconds formats it into an HH:mm:ss string
 * @memberof helpers
 * @param duration - the duration to format
 * @returns - formated string
 */
function formatDuration(duration: number): string {
    const hours = Math.floor(duration / 3_600_000);
    duration -= hours * 3_600_000;
    const minutes = Math.floor(duration / 60_000);
    duration -= minutes * 60_000;
    const seconds = Math.floor(duration / 1000);
    return `${hours}:${('0' + minutes).slice(-2)}:${('0' + seconds).slice(-2)}`;
}

// TODO: refactor this so it returns a string and prints it from the caller
/**
 * @function printTime
 * @description prints the time to console prettily
 * @memberof time
 * @param time - a Time object to print
 * @returns - no return value
 */
function printTime(time: Time): void {
    const duration = getDuration(
        new Date(time.startTime),
        time.endTime ? new Date(time.endTime) : new Date()
    );
    const tags = time.tags.map((tag: Tag) => tag.tag);
    outputter('\n\t', RED, 'Project:', '\t', CLEAR, time.projectName);

    outputter('\t', GREEN, 'Tags:', CLEAR, '\t', tags.join(','));
    outputter(
        '\t',
        ORANGE,
        'Start Time:',
        CLEAR,
        '\t',
        new Date(time.startTime).toString()
    );
    if (time.endTime) {
        outputter(
            '\t',
            DARK_BLUE,
            'End Time:',
            CLEAR,
            '\t',
            new Date(time.endTime).toString()
        );
    }
    outputter('\t', BLUE, 'Duration:', CLEAR, '\t', duration);
}

// TODO: refactor this so it can take in a date and get its week of year
/**
 * @function getWeek
 * @description get the current week of the year
 * @memberof helpers
 * @returns - the week of the year
 */
function getWeek(): number {
    const jan1 = new Date(new Date().getFullYear(), 0, 1);

    const numberOfDays = (Date.now() - jan1.getTime()) / (24 * 60 * 60 * 1000);

    return Math.ceil((new Date().getDay() + 1 + numberOfDays) / 7);
}

/**
 * @function groupByDate
 * @description groups an array of Times into dates
 * @memberof helpers
 * @param times - the times to group
 * @returns - array of array of times grouped by date
 */
function groupByDate(times: Time[]): Time[][] {
    const timeMap = new Map();
    for (const time of times) {
        const startTime = new Date(time.startTime);
        const date = `${startTime.getFullYear()}-${startTime.getMonth()}-${startTime.getDate()}`;
        if (!timeMap.has(date)) {
            timeMap.set(date, []);
        }
        timeMap.get(date).push(time);
    }
    return [...timeMap.values()];
}

/**
 * @function groupByProject
 * @description groups an array of Times into Projects
 * @memberof helpers
 * @param times - the times to group
 * @returns - array of array of times grouped by Projects
 */
function groupByProject(times: Time[]): Time[][] {
    const timeMap = new Map();
    for (const time of times) {
        if (!timeMap.has(time.projectName)) {
            timeMap.set(time.projectName, []);
        }
        timeMap.get(time.projectName).push(time);
    }
    return [...timeMap.values()];
}

function groupByTag(times: Time[]): Record<string, Time[]> {
    const tagMap: Record<string, Time[]> = {};
    for (const time of times) {
        for (const tag of time.tags) {
            if (!tagMap[tag.tag]) {
                tagMap[tag.tag] = [];
            }
            tagMap[tag.tag].push(time);
        }
    }
    return tagMap;
}

/**
 * @function totalDuration
 * @description totals all the times and returns its duration in miliseconds
 * @memberof helpers
 * @param times - array of times to total
 * @returns - the total duration in miliseconds
 */
function totalDuration(times: Time[]): number {
    return times.reduce(
        (accumulator, element) =>
            accumulator +
            Math.abs(
                new Date(element.startTime).getTime() -
                    (element.endTime
                        ? new Date(element.endTime)
                        : new Date()
                    ).getTime()
            ),
        0
    );
}

/**
 * @function formatDate
 * @description formats a date for display
 * @memberof helpers
 * @param date - the date to format
 * @returns - the date in `YYYY-MM-DD` format
 */
function formatDate(date: Date): string {
    return (
        String(date.getFullYear()) +
        '-' +
        String(date.getMonth() + 1).padStart(2, '0') +
        '-' +
        String(date.getDate()).padStart(2, '0')
    );
}

export default {
    outputter,
    getDuration,
    printTime,
    getWeek,
    groupByProject,
    groupByDate,
    totalDuration,
    formatDuration,
    groupByTag,
    formatDate,
    RED,
    GREEN,
    ORANGE,
    DARK_BLUE,
    BLUE,
    BOLD_UNDERLINE,
    CLEAR,
};
