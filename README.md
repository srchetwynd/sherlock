# Sherlock
> A time tracking CLI app track time spent on projects and tags.

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![Pipeline Status](https://gitlab.com/srchetwynd/sherlock/badges/master/pipeline.svg)](https://gitlab.com/srchetwynd/sherlock)
[![Code Coverage](https://gitlab.com/srchetwynd/sherlock/badges/master/coverage.svg)](https://gitlab.com/srchetwynd/sherlock)
[![License](https://img.shields.io/badge/license-MIT-green)](https://mit-license.org/)
[![Code Style](https://img.shields.io/badge/Code_Style-Prettier-ff69b4.svg)](https://prettier.io/)

Sherlock is a simple CLI program for tracking time, its inspired by [Watson](https://tailordev.github.io/Watson/). Its
possible to track time against project, and tags, and view your times grouped by project and day, in daily, weekly, and
monthly periods.

## Installation
To install sherlock you will need node.js and npm.


```sh
npm install -g @srchetwynd/sherlock
```

## Usage

Once installed the sherlock can be access using `sherlock` from the command line.

To show a list of all commands run:

```sh
sherlock --help
```

To list avaliable projects run:

```sh
sherlock projects
```

To add a new project run:

```sh
sherlock new-project
```

When starting to record a project it will stop any currently recording. To start recording time run:

```sh
sherlock start {project} [...tags]
````

To stop recording run:

```sh
sherlock stop
```

## Testing
Tests are stored in the `test` directory, the file structure mirrors the project structure.

## Versioning
This project follows the [SemVer](http://semver.org/)

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Donate
If you like this project and want to donate please click below:
[![Crypto donation button by NOWPayments](https://nowpayments.io/images/embeds/donation-button-black.svg)](https://nowpayments.io/donation?api_key=XSAXPKZ-ZH0MDR9-MT2WG9X-DXYYKRC)
