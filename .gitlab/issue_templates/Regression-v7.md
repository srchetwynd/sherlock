# v7 Regression

**Potential Commit/PR that introduced the regression**
If you have time to investigate, what PR/date introduced this issue.

**Describe the regression**
A clear and concise description of what the regression is.

**Expected behavior/code**
A clear and concise description of what you expected to happen (or code).

**Environment**
- Node/npm version: [e.g. Node 8/npm 5]
- OS: [e.g. OSX 10.13.4, Windows 10]
